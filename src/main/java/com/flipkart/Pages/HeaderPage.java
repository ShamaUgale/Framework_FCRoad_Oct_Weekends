package com.flipkart.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.flipkart.Utils.Keywords;

public class HeaderPage {

	Keywords actions= new Keywords();
	
	@FindBy(name="q")
	public WebElement searchTextBox;
	
	@FindBy(xpath="//button[@class='vh79eN' and @type='submit']")
	public WebElement searchBtn;
	
	
	
	/////////////////////  functions //////////////////
	
	
	public void searchProduct(String Productname){
		actions.type(searchTextBox, Productname);
		actions.click(searchBtn);
	}
	
	
}
