package com.flipkart.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.flipkart.Utils.Keywords;

public class ResultsPage {

	Keywords actions= new Keywords();
	
	@FindBy(xpath="//span[@class='W-gt5y']")
	public WebElement searchedProductText;
	
	
	/////////////////////////////
	
	public boolean verifyProductSearchResults(String expectedProductText){
		String actualText=actions.getElementText(searchedProductText);
		
		if(actualText.equalsIgnoreCase(expectedProductText)){
			return true;
		}else{
			return false;
		}
	}
	
	
}
