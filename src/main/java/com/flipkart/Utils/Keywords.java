package com.flipkart.Utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import com.flipkart.Base.TestBase;

public class Keywords extends TestBase{

	
	
	
	//openBrowser
	
	public void openBrowser(){
		
		String browser= CONFIG.getProperty("Browser");
		String runWithFirefoxProfile= CONFIG.getProperty("useFirefoxProfile");
		String firefoxProfile=CONFIG.getProperty("firefoxProfile");
		int  implicitWait=Integer.parseInt(CONFIG.getProperty("implicitWait"));
		
		
		
		DesiredCapabilities caps= new DesiredCapabilities();
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, true);
		caps.setCapability(CapabilityType.SUPPORTS_ALERTS, false);
		
				
		if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", PROJECT_PATH+"\\src\\main\\resources\\com\\flipkart\\drivers\\chromedriver.exe");
			driver= new ChromeDriver(caps);
		}else if(browser.equalsIgnoreCase("IE")){
			System.setProperty("webdriver.ie.driver", PROJECT_PATH+"\\src\\main\\resources\\com\\flipkart\\drivers\\IEServerDriver.exe");
			driver= new InternetExplorerDriver(caps);
		}else if(browser.equalsIgnoreCase("FF")){
			if(runWithFirefoxProfile.equalsIgnoreCase("true")){
				ProfilesIni profiles= new ProfilesIni();
				FirefoxProfile fp=profiles.getProfile(firefoxProfile);
				driver= new FirefoxDriver(fp);
			}else{
				driver= new FirefoxDriver(caps);
			}
		}else{
			System.setProperty("webdriver.chrome.driver", PROJECT_PATH+"\\src\\main\\resources\\com\\flipkart\\drivers\\chromedriver.exe");
			driver= new ChromeDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
	}
	
	
	public void navigateToURL(String URL){
		driver.navigate().to(URL);
		
	}
	
	public void click(WebElement elem){
		elem.click();
	}
	
	public void type(WebElement elem, String data){
		elem.clear();
		elem.sendKeys(data);
	}
	
	
	public String getElementText(WebElement elem){
		return elem.getText().trim();
	}
	
	public boolean verifyElementText(WebElement elem, String expectedText){
		String actualText= getElementText(elem);
		if(actualText.equalsIgnoreCase(expectedText)){
			return true;
		}else {
			return false;
		}
	}
	
	public void checkCheckBox(WebElement elem){
		if(! elem.isSelected()){
			click(elem);
		}
	}
	
	// uncheckCheckBox
	//verifyCheckBoxChecked
	//verifyCheckBoxUnChecked
	//selectRadio
	//verifyRadioSelected
	//isElementPresent
	//selectByText
	//selectByIndex
	//selectBtValueAttribute
	
	public void selectBtValueAttribute(WebElement elem , String valueAttribute){
		Select sec= new Select(elem);
		sec.selectByValue(valueAttribute);
	}
	
	//getAlertText
	//acceptAlert
	//dismissAlert
	//getCurrentPageTitle
	//getCurrentURL
	//wait function
	
	
	
	
	
	
}
