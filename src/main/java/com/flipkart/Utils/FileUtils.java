package com.flipkart.Utils;

public class FileUtils {

	
	
	public static Object[][] getTestData(String filePath,String Sheetname){
		
		Xls_Reader xls= new Xls_Reader(filePath);
		int rows= xls.getRowCount(Sheetname);//12
		int cols = xls.getColumnCount(Sheetname);
		
		Object[][] data= new Object[rows-1][cols];
		
		for(int i=0;i<data.length;i++){
			for(int j=0;j<data[0].length;j++){
				data[i][j]=xls.getCellData(Sheetname, j, i+1);
			}
		}
		
		return data;
		 
		
		
		
	}

}
