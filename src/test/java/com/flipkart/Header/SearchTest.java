package com.flipkart.Header;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.flipkart.Base.TestBase;
import com.flipkart.Pages.HeaderPage;
import com.flipkart.Pages.ResultsPage;
import com.flipkart.Utils.FileUtils;
import com.flipkart.Utils.Keywords;

public class SearchTest extends TestBase{

	
	@BeforeSuite
	public void setup() throws IOException{
		init();
		Keywords actions= new Keywords();
		actions.openBrowser();
		actions.navigateToURL(CONFIG.getProperty("TestUrl"));
	}
	
	
	
	@Test(dataProvider="getTestData")
	public void searchProductTest(String Productname){
		
		HeaderPage h= PageFactory.initElements(driver, HeaderPage.class);
		h.searchProduct(Productname);
		ResultsPage res= PageFactory.initElements(driver, ResultsPage.class);
		boolean isResultDisplayed=res.verifyProductSearchResults(Productname);
		
		Assert.assertTrue(isResultDisplayed, "Results were not displayed for : "+ Productname);
		
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return FileUtils.getTestData(PROJECT_PATH+"\\src\\test\\resources\\com\\flipkart\\Tests\\SearchTest.xlsx", "SearchTest");
	}
	
}
